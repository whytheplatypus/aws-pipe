GOTOOLS = \
	github.com/kardianos/govendor \
	github.com/jstemmer/go-junit-report \
	github.com/golang/lint/golint
BIN_DIR=bin
SCRIPTS=scripts
DIST=dist
DEVOPS_DIST=.devops/dist
PACKAGES=$(shell go list ./... | grep -v '^github.com/riskive/go-aws/vendor/')
NEEDS_FORMAT=$(shell gofmt -l . | grep -v '^vendor')
PACKAGE_NAME=$(shell go list .)

all: build

format:
	go fmt $(PACKAGES)

check-fmt:
	@if [ "$(NEEDS_FORMAT)" != "" ] ; then \
		echo $(NEEDS_FORMAT); \
		exit 1; \
	fi

test:
	go test

save: tools
	govendor add +external

get: tools
	govendor fetch +out

lint: tools
	@if [ "`golint ./... | grep -v -e '^vendor' -e '\.pb\.go' -e '^examples'`" != "" ] ; then\
		echo `golint ./... | grep -v -e '^vendor' -e '\.pb\.go' -e '^examples'`; \
		exit 1; \
	fi \

tools:
	go get -u -v $(GOTOOLS)

docs: 
	$(foreach pkg,$(PACKAGES),\
		godoc --templates=$$GODOC_TEMPLATES $(pkg) | sed 's/\/target\///' > $$GOPATH/src/$(pkg)/README.md;)

.PHONY: test lint get save tools 
