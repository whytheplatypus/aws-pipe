// Package pipe provides tools that help sqs behave something akin to the go standard http lib.
//
// [![GoDoc](https://godoc.org/gitlab.com/whytheplatypus/aws-pipe?status.svg)](https://godoc.org/gitlab.com/whytheplatypus/aws-pipe)
package pipe
