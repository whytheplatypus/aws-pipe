package awstest

import (
	"bytes"
	"crypto/md5"
	"encoding/hex"
	"encoding/xml"
	"io/ioutil"
	"net/http"
	"net/http/httputil"
	"testing"
	"time"

	"github.com/aws/aws-sdk-go/aws"
	"github.com/aws/aws-sdk-go/aws/request"
	"github.com/aws/aws-sdk-go/service/sqs"
)

// SQS represents a server that mimics a aws SQS instance
type SQS struct {
	Messages map[string](chan []byte)
	T        *testing.T
}

func (s *SQS) ServeHTTP(w http.ResponseWriter, r *http.Request) {
	dr, _ := httputil.DumpRequest(r, true)
	if s.T != nil {
		s.T.Logf("[TEST] sqs request : %s", string(dr))
	}
	action := r.FormValue("Action")
	QURL := r.FormValue("QueueUrl")
	switch action {
	case "ReceiveMessage":
		select {
		case msg := <-s.Messages[QURL]:
			if s.T != nil {
				s.T.Logf("[TEST] getting message %s off fake queue %s", msg, QURL)
			}
			if _, err := w.Write(msg); err != nil {
				if s.T != nil {
					s.T.Fatalf("[ERROR] could not write message to http server: %v", err.Error())
				}
				return
			}

		// waiting one second to ensure command can read all messages
		case <-time.After(1 * time.Second):
		}
	case "SendMessage":
		e := xml.NewEncoder(w)
		smo := &request.Request{}
		body := ioutil.NopCloser(bytes.NewReader([]byte("")))
		smo.HTTPResponse = &http.Response{StatusCode: 200, Body: body}
		sum := signMessage(r.FormValue("MessageBody"))
		xmlMsg, err := buildMessage(r.FormValue("MessageBody"), sum)

		if err != nil {
			s.T.Fatalf("[TEST ERROR] failed to create sqs message : %s", err.Error())
			return
		}
		s.Messages[QURL] <- xmlMsg
		smo.Data = &sqs.SendMessageOutput{
			MD5OfMessageBody: aws.String(sum),
			MessageId:        aws.String("12345"),
		}

		//smo := &sqs.SendMessageOutput{}
		//smo.SetMD5OfMessageBody(fmt.Sprintf("%x", md5.Sum([]byte(r.FormValue("MessageBody")))))
		if err := e.Encode(smo.Data); err != nil {
			w.WriteHeader(http.StatusBadRequest)
			s.T.Logf("[TEST ERROR] failed to write SendMessageOutput : %s", err.Error())
			return
		}
	}
}

// BuildSQSMessage constructs the struct expected for a simple SQS message
func BuildSQSMessage(msg string) ([]byte, error) {
	sum := signMessage(msg)
	sqsMsg := &ReceiveMessageResponse{
		ReceiveMessageResult: Messages{
			Message: []Message{
				Message{
					Body:      msg,
					MD5OfBody: sum,
				},
			},
		},
	}

	xmlMsg, err := xml.Marshal(sqsMsg)
	return xmlMsg, err
}

func signMessage(msg string) string {
	msum := md5.Sum([]byte(msg))
	sum := hex.EncodeToString(msum[:])
	return sum
}

func buildMessage(msg string, sum string) ([]byte, error) {
	sqsMsg := &ReceiveMessageResponse{
		ReceiveMessageResult: Messages{
			Message: []Message{
				Message{
					Body:      msg,
					MD5OfBody: sum,
				},
			},
		},
	}

	xmlMsg, err := xml.Marshal(sqsMsg)
	return xmlMsg, err
}

/*
	<ReceiveMessageResponse>
		<ReceiveMessageResult>
			<Message>
				<MessageId>
					5fea7756-0ea4-451a-a703-a558b933e274
				</MessageId>
				<ReceiptHandle>
					MbZj6wDWli+JvwwJaBV+3dcjk2YW2vA3+STFFljTM8tJJg6HRG6PYSasuWXPJB+Cw
					Lj1FjgXUv1uSj1gUPAWV66FU/WeR4mq2OKpEGYWbnLmpRCJVAyeMjeU5ZBdtcQ+QE
					auMZc8ZRv37sIW2iJKq3M9MFx1YvV11A2x/KSbkJ0=
				</ReceiptHandle>
				<MD5OfBody>
					fafb00f5732ab283681e124bf8747ed1
				</MD5OfBody>
				<Body>This is a test message</Body>
				<Attribute>
					<Name>SenderId</Name>
					<Value>195004372649</Value>
				</Attribute>
				<Attribute>
					<Name>SentTimestamp</Name>
					<Value>1238099229000</Value>
				</Attribute>
				<Attribute>
					<Name>ApproximateReceiveCount</Name>
					<Value>5</Value>
				</Attribute>
				<Attribute>
					<Name>ApproximateFirstReceiveTimestamp</Name>
					<Value>1250700979248</Value>
				</Attribute>
			</Message>
		</ReceiveMessageResult>
		<ResponseMetadata>
			<RequestId>
				b6633655-283d-45b4-aee4-4e84e0ae6afa
			</RequestId>
		</ResponseMetadata>
	</ReceiveMessageResponse>
*/

// ReceiveMessageResponse is a xml type in a simple SQS message
type ReceiveMessageResponse struct {
	ReceiveMessageResult Messages
}

// Messages is a xml type in a simple SQS message
type Messages struct {
	Message []Message
}

// Message is a xml type in a simple SQS message
type Message struct {
	Body      string
	MD5OfBody string
}
