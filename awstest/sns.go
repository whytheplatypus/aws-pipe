package awstest

import (
	"log"
	"net/http"
	"net/http/httputil"
	"testing"
)

// SNS represents a http server that behaves similarly to an aws SNS instance.
type SNS struct {
	T             *testing.T
	Subscriptions map[string][](chan []byte)
}

func (s *SNS) ServeHTTP(w http.ResponseWriter, r *http.Request) {
	dr, _ := httputil.DumpRequest(r, true)
	log.Printf("[TEST] sns request : %s", string(dr))
	action := r.FormValue("Action")
	arn := r.FormValue("TopicArn")
	//QURL := r.FormValue("QueueUrl")
	switch action {
	case "Publish":
		msg, err := BuildSQSMessage(r.FormValue("Message"))
		if err != nil {
			s.T.Fatalf("[TEST ERROR] failed to send sns message : %s", err.Error())
			return
		}
		for _, sub := range s.Subscriptions[arn] {
			sub <- msg
		}
	}
}

// Subscribe allows a awstest.SQS instance to subscribe to this SNS instance.
func (s *SNS) Subscribe(q *SQS, url string, arn string) {
	s.Subscriptions[arn] = append(s.Subscriptions[arn], q.Messages[url])
}
