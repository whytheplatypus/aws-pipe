
`import gitlab.com/whytheplatypus/aws-pipe/awstest`




# Functions


## [BuildSQSMessage](sqs.go#L78)
```go
func BuildSQSMessage(msg string) ([]byte, error)
```
BuildSQSMessage constructs the struct expected for a simple SQS message






# Types


## [Message](sqs.go#L170)
Message is a xml type in a simple SQS message

```go
type Message struct {
    Body      string
    MD5OfBody string
}
```









## [Messages](sqs.go#L165)
Messages is a xml type in a simple SQS message

```go
type Messages struct {
    Message []Message
}
```









## [ReceiveMessageResponse](sqs.go#L160)
ReceiveMessageResponse is a xml type in a simple SQS message

```go
type ReceiveMessageResponse struct {
    ReceiveMessageResult Messages
}
```









## [SNS](sns.go#L11)
SNS represents a http server that behaves similarly to an aws SNS instance.

```go
type SNS struct {
    T             *testing.T
    Subscriptions map[string][](chan []byte)
}
```







### [ServeHTTP](sns.go#L16)
```go
func (s *SNS) ServeHTTP(w http.ResponseWriter, r *http.Request)
```





### [Subscribe](sns.go#L36)
```go
func (s *SNS) Subscribe(q *SQS, url string, arn string)
```
Subscribe allows a awstest.SQS instance to subscribe to this SNS instance.







## [SQS](sqs.go#L20)
SQS represents a server that mimics a aws SQS instance

```go
type SQS struct {
    Messages map[string](chan []byte)
    T        *testing.T
}
```







### [ServeHTTP](sqs.go#L25)
```go
func (s *SQS) ServeHTTP(w http.ResponseWriter, r *http.Request)
```










