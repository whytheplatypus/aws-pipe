package pipe_test

import (
	"fmt"
	"log"
	"net/http/httptest"
	"os"
	"testing"

	"github.com/aws/aws-sdk-go/aws"
	"github.com/aws/aws-sdk-go/aws/credentials"
	"github.com/aws/aws-sdk-go/aws/session"
	"github.com/aws/aws-sdk-go/service/sqs"
	"github.com/stretchr/testify/assert"
	pipe "gitlab.com/whytheplatypus/aws-pipe"
	"gitlab.com/whytheplatypus/aws-pipe/awstest"
)

func ExampleSQS() {

	os.Setenv("AWS_ACCESS_KEY", "bogus")
	os.Setenv("AWS_SECRET_ACCESS_KEY", "bogus")
	os.Setenv("AWS_REGION", "us-west-2")

	writer, err := pipe.NewSNS("some-topic")

	if err != nil {
		log.Fatal(err)
	}

	byteStr := []byte("here be a string matey")

	writer.Write(byteStr)

	outHandler := func(m *sqs.Message, s string) {
		writer.Write([]byte(*m.Body))
	}

	if err := pipe.Listen("my-queue-url", outHandler); err != nil {
		log.Fatal(err)
	}
}

func TestSQSRead_LongBody(t *testing.T) {

	longBody := ""

	for i := 0; i < 2048; i++ {
		longBody = fmt.Sprintf("%s%s", longBody, "test")
	}

	tsqs := &awstest.SQS{
		T:        t,
		Messages: map[string](chan []byte){},
	}

	srv := httptest.NewServer(tsqs)
	defer func() {
		srv.CloseClientConnections()
		srv.Close()
	}()

	sess, err := session.NewSession()
	if err != nil {
		t.Fatalf("[ERROR] could not create session to aws : %s", err.Error())
	}

	tsqs.Messages[srv.URL] = make(chan []byte, 100)

	sqsSvc := sqs.New(sess, &aws.Config{
		Credentials: credentials.NewStaticCredentials("bogus", "bogus", ""),
		Endpoint:    &srv.URL,
		Region:      aws.String("us-west-2"),
	})

	params := &sqs.ReceiveMessageInput{
		MaxNumberOfMessages: aws.Int64(10),
		WaitTimeSeconds:     aws.Int64(20),
		QueueUrl:            &srv.URL,
	}

	r := &pipe.SQS{
		SQSAPI: sqsSvc,
	}

	w := &pipe.Writer{
		SQSAPI:   sqsSvc,
		QueueUrl: srv.URL,
	}

	w.Write([]byte(longBody))
	testHandler := func(m *sqs.Message, s string) {
		assert.Equal(t, longBody, *m.Body)

		r.Shutdown()
	}

	if err := r.Listen(params, testHandler); err != nil {
		t.Fatal(err)
	}
}
