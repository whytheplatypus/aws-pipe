package pipe_test

import (
	"log"
	"net/http/httptest"
	"os"
	"testing"

	"github.com/aws/aws-sdk-go/service/sqs"
	"github.com/stretchr/testify/assert"
	aws "gitlab.com/whytheplatypus/aws-pipe"
	"gitlab.com/whytheplatypus/aws-pipe/awstest"
)

func TestSNS(t *testing.T) {
	tsns := &awstest.SNS{
		Subscriptions: map[string][](chan []byte){
			"lol": [](chan []byte){},
		},
	}

	srv := httptest.NewServer(tsns)
	defer func() {
		srv.CloseClientConnections()
		srv.Close()
	}()
	tsqs := &awstest.SQS{
		Messages: map[string](chan []byte){},
	}

	srvSQS := httptest.NewServer(tsqs)
	defer func() {
		srvSQS.CloseClientConnections()
		srvSQS.Close()
	}()

	tsqs.Messages[srvSQS.URL] = make(chan []byte, 100)
	tsns.Subscribe(tsqs, srvSQS.URL, "lol")

	os.Setenv("AWS_ACCESS_KEY", "bogus")
	os.Setenv("AWS_SECRET_ACCESS_KEY", "bogus")
	os.Setenv("AWS_SNS_URL", srv.URL)
	os.Setenv("AWS_SQS_URL", srvSQS.URL)
	os.Setenv("AWS_REGION", "us-west-2")
	writer, err := aws.NewSNS("lol")
	if err != nil {
		log.Fatal(err)
	}

	byteStr := []byte("here be a string matey")

	writer.Write(byteStr)
	done := make(chan struct{})
	testHandler := func(m *sqs.Message, s string) {
		assert.Equal(t, "here be a string matey", *m.Body)
		done <- struct{}{}
	}
	go func() {
		if err := aws.Listen(srvSQS.URL, testHandler); err != nil {
			log.Fatal(err)
		}
	}()
	<-done
}
