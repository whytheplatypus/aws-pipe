Package pipe provides tools that help sqs behave something akin to the go
standard http lib.

[![GoDoc](https://godoc.org/gitlab.com/whytheplatypus/aws-pipe?status.svg)](https://godoc.org/gitlab.com/whytheplatypus/aws-pipe)




```go
package main

import (
    "log"
    "os"
    "github.com/aws/aws-sdk-go/service/sqs"
    pipe "gitlab.com/whytheplatypus/aws-pipe"
)

func main() {

    os.Setenv("AWS_ACCESS_KEY", "bogus")
    os.Setenv("AWS_SECRET_ACCESS_KEY", "bogus")
    os.Setenv("AWS_REGION", "us-west-2")

    writer, err := pipe.NewSNS("some-topic")

    if err != nil {
        log.Fatal(err)
    }

    byteStr := []byte("here be a string matey")

    writer.Write(byteStr)

    outHandler := func(m *sqs.Message, s string) {
        writer.Write([]byte(*m.Body))
    }

    if err := pipe.Listen("my-queue-url", outHandler); err != nil {
        log.Fatal(err)
    }
}

```



`import gitlab.com/whytheplatypus/aws-pipe`



# Variables

ErrEmptyMessage represents an error when the sqs queue being read from is empty

```go
var ErrEmptyMessage = errors.New("The SQS queue is currently empty")
```



# Functions


## [Listen](sqs.go#L154)
```go
func Listen(url string, h Handler) error
```
Listen begins listening to the messages from the sqs queue specified and calls a
Handler for each one





## [NewReader](sqs.go#L36)
```go
func NewReader(m sqs.Message) *strings.Reader
```
NewReader returns an io.Reader (*strings.Reader) created from the body of the
sqs.Message





## [NewSNSService](sns.go#L61)
```go
func NewSNSService() (*sns.SNS, error)
```
NewSNSService returns a new SNS service with set endpoint/region






# Types


## [Handler](sqs.go#L32)
Handler is a function that can do something with an sqs.Message and the queue
url it came from

```go
type Handler func(*sqs.Message, string)
```









## [SNS](sns.go#L19)
SNS satisfies the io.Writer interface and extends SNSAPI interface to send
protobuf over SNS

```go
type SNS struct {
    MessageAttributes map[string]*sns.MessageAttributeValue
    // contains filtered or unexported fields
}
```






### [NewSNS](sns.go#L48)
```go
func NewSNS(topicARN string) (*SNS, error)
```
NewSNS returns a new SNS struct with set client/topicARN





### [Write](sns.go#L30)
```go
func (w *SNS) Write(p []byte) (int, error)
```
* Write satisfies the io.Writer interface and does the following:

	1. Convert incoming protobuf byte string to base 64
	2. Publish base64 encoding protobuf to SNS







## [SQS](sqs.go#L24)
SQS is a reader for an sqs queue

```go
type SQS struct {
    sqsiface.SQSAPI
    // contains filtered or unexported fields
}
```







### [Listen](sqs.go#L41)
```go
func (s *SQS) Listen(rmi *sqs.ReceiveMessageInput, h Handler) (err error)
```
Listen begins listening to the messages from the sqs queue specified and calls a
Handler for each one





### [Shutdown](sqs.go#L95)
```go
func (s *SQS) Shutdown() error
```
Shutdown triggers a clean shutdown of the running sqs queue Listen command TODO
accept a context much like
[http.Shutdown](https://golang.org/pkg/net/http/#Server.Shutdown)







## [Writer](sqs.go#L104)
Writer allows an sqs queue url to be treated like an io.Writer

```go
type Writer struct {
    QueueUrl string
    sqsiface.SQSAPI
}
```






### [NewWriter](sqs.go#L122)
```go
func NewWriter(url string) (*Writer, error)
```
NewWriter creates and returns a new Writer for a given queue url





### [Write](sqs.go#L109)
```go
func (s *Writer) Write(p []byte) (n int, err error)
```










# Packages

	
[awstest](\awstest)  
	


