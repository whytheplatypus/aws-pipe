package pipe

import (
	"fmt"
	"log"
	"os"

	"github.com/aws/aws-sdk-go/aws"
	"github.com/aws/aws-sdk-go/aws/credentials"
	"github.com/aws/aws-sdk-go/aws/credentials/ec2rolecreds"
	"github.com/aws/aws-sdk-go/aws/ec2metadata"
	"github.com/aws/aws-sdk-go/aws/session"
	"github.com/aws/aws-sdk-go/service/sns"
	"github.com/aws/aws-sdk-go/service/sns/snsiface"
)

// SNS satisfies the io.Writer interface and extends SNSAPI interface to send
// protobuf over SNS
type SNS struct {
	snsClient         snsiface.SNSAPI
	topicARN          *string
	MessageAttributes map[string]*sns.MessageAttributeValue
}

/**
Write satisfies the io.Writer interface and does the following:
	1. Convert incoming protobuf byte string to base 64
	2. Publish base64 encoding protobuf to SNS
*/
func (w *SNS) Write(p []byte) (int, error) {

	snsPublishParams := &sns.PublishInput{
		Message:           aws.String(string(p)),
		TopicArn:          w.topicARN,
		MessageAttributes: w.MessageAttributes,
	}
	log.Printf("[DEBUG] writing to sns : %s : %v", string(p), w.MessageAttributes)
	if _, err := w.snsClient.Publish(snsPublishParams); err != nil {
		log.Printf("[ERROR] failed to publish message to SNS: %s", err.Error())

		return 0, err
	}

	return len(p), nil
}

// NewSNS returns a new SNS struct with set client/topicARN
func NewSNS(topicARN string) (*SNS, error) {
	snsC, err := NewSNSService()
	if err != nil {
		return nil, err
	}
	return &SNS{
		snsClient:         snsC,
		topicARN:          aws.String(topicARN),
		MessageAttributes: map[string]*sns.MessageAttributeValue{},
	}, nil
}

// NewSNSService returns a new SNS service with set endpoint/region
func NewSNSService() (*sns.SNS, error) {
	snsEndpoint := os.Getenv("AWS_SNS_URL")
	sess, err := session.NewSession()
	if err != nil {
		return nil, fmt.Errorf("could not create session to aws: %s", err.Error())
	}

	awsCreds := credentials.NewChainCredentials([]credentials.Provider{
		&credentials.EnvProvider{},
		&ec2rolecreds.EC2RoleProvider{
			Client: ec2metadata.New(sess),
		},
	})

	return sns.New(sess, &aws.Config{
		Credentials: awsCreds,
		Endpoint:    aws.String(snsEndpoint),
	}), nil
}
