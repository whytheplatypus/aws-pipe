package pipe

import (
	"errors"
	"fmt"
	"log"
	"os"
	"strings"
	"sync"

	"github.com/aws/aws-sdk-go/aws"
	"github.com/aws/aws-sdk-go/aws/credentials"
	"github.com/aws/aws-sdk-go/aws/credentials/ec2rolecreds"
	"github.com/aws/aws-sdk-go/aws/ec2metadata"
	"github.com/aws/aws-sdk-go/aws/session"
	"github.com/aws/aws-sdk-go/service/sqs"
	"github.com/aws/aws-sdk-go/service/sqs/sqsiface"
)

// ErrEmptyMessage represents an error when the sqs queue being read from is empty
var ErrEmptyMessage = errors.New("The SQS queue is currently empty")

// SQS is a reader for an sqs queue
type SQS struct {
	sqsiface.SQSAPI
	done chan struct{}
}

const defaultRegion = "us-west-2"

// Handler is a function that can do something with an sqs.Message and the queue url it came from
type Handler func(*sqs.Message, string)

// NewReader returns an io.Reader (*strings.Reader)
// created from the body of the sqs.Message
func NewReader(m sqs.Message) *strings.Reader {
	return strings.NewReader(*m.Body)
}

// Listen begins listening to the messages from the sqs queue specified and calls a Handler for each one
func (s *SQS) Listen(rmi *sqs.ReceiveMessageInput, h Handler) (err error) {
	// 10 is the max number of messages in a response from sqs
	// http://docs.aws.amazon.com/AWSSimpleQueueService/latest/APIReference/API_ReceiveMessage.html
	var numH = 10

	mc := make(chan *sqs.Message, numH)

	var wg sync.WaitGroup

	for i := 0; i < numH; i++ {
		wg.Add(1)
		go func(mc <-chan *sqs.Message, num int) {
			defer func() {
				if panic := recover(); panic != nil {
					log.Printf("[ERROR] panic in handler : %s", panic)
				}
			}()
			defer wg.Done()

			for m := range mc {
				h(m, *rmi.QueueUrl)
			}
			log.Printf("[INFO] exiting handler %d", num)
		}(mc, i)
	}
	for {
		select {
		case <-s.done:
			log.Println("[INFO] shutting down sqs listener")
		default:
			resp, err := s.ReceiveMessage(rmi)

			if err != nil {
				e := fmt.Errorf("[ERROR] Unable to read messages [ %s ]", err.Error())
				log.Println(e)
				break
			}
			for _, msg := range resp.Messages {
				//log.Printf("[DEBUG] Got SQS Message : %s", *msg.Body)
				mc <- msg
			}
			continue
		}
		break
	}

	//TODO add a context here that hard exists this function if it beats Wait()
	close(mc)
	wg.Wait()
	return err
}

// Shutdown triggers a clean shutdown of the running sqs queue Listen command
// TODO accept a context much like [http.Shutdown](https://golang.org/pkg/net/http/#Server.Shutdown)
func (s *SQS) Shutdown() error {
	if s.done == nil {
		s.done = make(chan struct{})
	}
	s.done <- struct{}{}
	return nil
}

// Writer allows an sqs queue url to be treated like an io.Writer
type Writer struct {
	QueueUrl string
	sqsiface.SQSAPI
}

func (s *Writer) Write(p []byte) (n int, err error) {
	in := &sqs.SendMessageInput{
		QueueUrl:    aws.String(s.QueueUrl),
		MessageBody: aws.String(string(p)),
	}
	if _, err := s.SendMessage(in); err != nil {
		log.Printf("[ERROR] Unable to write message to queue : %v : %s", string(p), err.Error())
		return n, err
	}
	return len(p), nil
}

// NewWriter creates and returns a new Writer for a given queue url
func NewWriter(url string) (*Writer, error) {
	c, err := newSQSService()
	return &Writer{
		QueueUrl: url,
		SQSAPI:   c,
	}, err
}

// NewSQSService returns a new SQS service with set endpoint/region
func newSQSService() (*sqs.SQS, error) {
	sqsEndpoint := os.Getenv("AWS_SQS_URL")
	sess, err := session.NewSession()

	if err != nil {
		return nil, fmt.Errorf("could not create session to aws: %s", err.Error())
	}

	awsCreds := credentials.NewChainCredentials([]credentials.Provider{
		&credentials.EnvProvider{},
		&ec2rolecreds.EC2RoleProvider{
			Client: ec2metadata.New(sess),
		},
	})

	return sqs.New(sess, &aws.Config{
		Logger:      nil,
		Credentials: awsCreds,
		Endpoint:    aws.String(sqsEndpoint),
	}), nil
}

// Listen begins listening to the messages from the sqs queue specified and calls a Handler for each one
func Listen(url string, h Handler) error {
	sqsC, err := newSQSService()
	if err != nil {
		return err
	}
	s := &SQS{
		SQSAPI: sqsC,
	}
	return s.Listen(&sqs.ReceiveMessageInput{
		MaxNumberOfMessages: aws.Int64(10),
		WaitTimeSeconds:     aws.Int64(20),
		QueueUrl:            aws.String(url),
	}, h)
}
